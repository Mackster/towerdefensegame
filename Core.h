#pragma once
#pragma GCC diagnostic ignored "-Wnarrowing"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

typedef unsigned int uint32;

typedef char byte;
typedef unsigned char ubyte;