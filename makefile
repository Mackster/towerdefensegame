
LIBS = -lSDL2 -lSDL2_image -lSDL2_ttf
FILES = *.cpp
OUT_NAME = game.out
BINARY_NAME = tdg
VERSION = -std=c++11
default: compile

clean:
	rm $(OUT_NAME) && sudo rm -f /usr/bin/$(BINARY_NAME)
install: compile
	sudo cp -f $(OUT_NAME) /usr/bin/$(BINARY_NAME)
compile:
	g++ $(VERSION) $(FILES) $(LIBS) -o $(OUT_NAME)
