#pragma once

#include "Core.h"
#include "Colour.h"
#include "Math.h"
#include "util.h"

#define FRAME_VALUES 10

class Window
{
public:
	Window(uint32 width, uint32 height);
	Window(uint32 width, uint32 height, const char* title);
	Window(uint32 width, uint32 height, Colour background);
	Window(uint32 width, uint32 height, const char* title, Colour background);
	~Window();

private:
	uint32 m_width;
	uint32 m_height;

	const char* m_title;

	SDL_Window* m_Screen;
	SDL_Surface* m_MainSurface;

	SDL_Renderer* m_MainRenderer;

	bool Running = true;

	void (*UpdateCallback)() = 0;
	void (*DrawGFX)() = 0;

	int lastMillis = 0;

public:
	uint32 GetWidth() const { return m_width; }
	uint32 GetHeight() const { return m_height; }

	Vector2 GetMouseCoords();

	bool GetKey(SDL_Keycode key);
	bool MouseClicked() { return currentEvent.type == SDL_MOUSEBUTTONDOWN && lastEvent.type != SDL_MOUSEBUTTONDOWN; }
	bool MouseHeld() { return currentEvent.type == SDL_MOUSEBUTTONDOWN; }

	int GetFPS() { return (int)framespersecond; }

	SDL_Surface *GetWindowSurface() { return m_MainSurface; }
	SDL_Renderer *GetWindowRenderer() { return m_MainRenderer; }

	void Run();
	void Quit() { Running = false; }

	void SetUpdateCallback(void (*func)()) { UpdateCallback = func; }
	void SetGFXCallback(void (*func)()) { DrawGFX = func; }

	SDL_Event currentEvent;
	SDL_Event lastEvent;

	Uint64 NOW = 0;
	Uint64 LAST = 0;
	double DeltaTime = 0;

	//Framerate crap
	Uint32 frametimes[FRAME_VALUES];
	Uint32 frametimelast;
	Uint32 framecount;
	float framespersecond;

	void FpsInit()
	{
		memset(frametimes, 0, sizeof(frametimes));
		framecount = 0;
		framespersecond = 0;
		frametimelast = SDL_GetTicks();

		NOW = SDL_GetPerformanceCounter();
	}

	void FpsThink()
	{
		Uint32 frametimesindex;
		Uint32 getticks;
		Uint32 count;
		Uint32 i;

		frametimesindex = framecount % FRAME_VALUES;

		getticks = SDL_GetTicks();
		frametimes[frametimesindex] = getticks - frametimelast;

		frametimelast = getticks;
		framecount++;

		if (framecount < FRAME_VALUES)
		{
			count = framecount;
		}
		else
		{
			count = FRAME_VALUES;
		}

		framespersecond = 0;

		for (i = 0; i < count; i++)
		{
			framespersecond += frametimes[i];
		}

		framespersecond /= count;
		framespersecond = 1000.f / framespersecond;
	}
};