#pragma once

#include "Core.h"
#include <string>
#include "Math.h"

class Texture
{
private:
    SDL_Surface *m_tex;

public:
    Texture();
    Texture(const char *path) { this->LoadFromFile(path); }
    Texture(std::string path) { this->LoadFromFile(path); }
    ~Texture();

    uint32 GetHeight() { return m_tex->h; }
    uint32 GetWidth() { return m_tex->w; }

    bool LoadFromFile(std::string path);
    bool OptimizeForWindow(SDL_Surface *win);

    void Render(SDL_Renderer *rend);
    void Render(SDL_Renderer *rend, const Vector2 &pos, const Vector2 &size);
};