#pragma once
#include "Core.h"
#include "Math.h"

class UIConstraint
{
public:
    virtual Vector2 Constrain(Vector2 &vec) { return Vec2One; };
    virtual int Constrain(int &value) { return 1; };

    virtual char *GetType() { return "BASE"; };
};

class VectorConstraint : public UIConstraint
{
public:
    double minX, maxX;
    double minY, maxY;

    char *target;

    VectorConstraint(const double &_minX, const double &_maxX, const double &_minY, const double &_maxY, char *_target)
        : minX(_minX), maxX(_maxX), minY(_minY), maxY(_maxY), target(_target) {}

    ~VectorConstraint();

    Vector2 Constrain(Vector2 &vec) override
    {
        if (vec.x < minX)
            vec.x = minX;

        if (vec.x > maxX)
            vec.x = maxX;

        if (vec.y < minY)
            vec.y = minY;

        if (vec.y > minY)
            vec.y = minY;
    }

    char *GetType() override
    {
        return target;
    }

    int Constrain(int &value) override { return value; }
};