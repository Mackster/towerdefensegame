#pragma once

#include "Core.h"

#include "Math.h"
#include "Colour.h"
#include <string>

static std::string ToString(const int& a)
{
    return std::to_string(a);
}

//Convert a double to a string with a specified number of decimal places
static std::string ToString(const double& a, const int& decimalPlaces)
{
    std::string ret;
    int counter = 0;
    bool found = false;

    for(char& c : std::to_string(a))
    {
        if(found)
            counter++;

        if(c != '.')
        {
            ret += c;
        }
        else
        {
            found = true;
            if(decimalPlaces > 0)
                ret += c;
        }

        if(counter == decimalPlaces)
            break;
    }

    return ret;
}

static int slowLogCounter = 0;

static void SlowLog(int amount, const char* msg)
{
    slowLogCounter++;

    if(slowLogCounter == amount)
    {
        printf(msg);
        slowLogCounter = 0;
    }
}