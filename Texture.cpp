#include "Texture.h"

Texture::~Texture()
{
    SDL_FreeSurface(m_tex);
}

bool Texture::LoadFromFile(std::string path)
{
    m_tex = IMG_Load(path.c_str());

    if (m_tex == NULL)
    {
        printf("Failed to load image: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

bool Texture::OptimizeForWindow(SDL_Surface *surf)
{
    m_tex = SDL_ConvertSurface(m_tex, surf->format, 0);

    if (m_tex == NULL)
    {
        printf("Failed to convert surface: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

void Texture::Render(SDL_Renderer *rend)
{
    if (m_tex == 0)
    {
        return;
    }

    SDL_Texture *temp = SDL_CreateTextureFromSurface(rend, m_tex);

    SDL_RenderCopy(rend, temp, &(m_tex->clip_rect), nullptr);

    SDL_DestroyTexture(temp);
}

void Texture::Render(SDL_Renderer *rend, const Vector2 &position, const Vector2 &size)
{
    if (m_tex == 0)
    {
        return;
    }

    SDL_Texture *temp = SDL_CreateTextureFromSurface(rend, m_tex);

    SDL_Rect pos = {position.x, position.y, size.x, size.y};

    SDL_RenderCopy(rend, temp, &(m_tex->clip_rect), &pos);

    SDL_DestroyTexture(temp);
}