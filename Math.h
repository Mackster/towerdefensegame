#pragma once

#include "Core.h"
#include "Colour.h"
#include <cmath>

#define Vec2Zero Vector2(0, 0)
#define Vec2One Vector2(1, 1)

#define Vec3Zero Vector3(0, 0, 0)
#define Vec3One Vector3(1, 1, 1)

class Vector2
{
public:
    double x;
    double y;

    Vector2(double x, double y)
        : x(x), y(y) {}
    ~Vector2(){};

    double GetLength()
    {
        return sqrt(x * x + y * y);
    }

    double GetSqrLength()
    {
        return x * x + y + y;
    }

    double GetDistance(Vector2& a, Vector2& b)
    {
        return (a - b).GetLength();
    }

    double GetSqrDistance(Vector2& a, Vector2& b)
    {
        return (a - b).GetSqrLength();
    }

    Vector2 operator+(const Vector2& a)
    {
        return Vector2(x + a.x, y + a.y);
    }

    Vector2 operator-(const Vector2& a)
    {
        return Vector2(x - a.x, y - a.y);
    }

    void operator+=(const Vector2& a)
    {
        *this = *this + a;
    }

    void operator-=(const Vector2& a)
    {
        *this = *this - a;
    }
};

class Vector3
{
public:
    double m_x;
    double m_y;
    double m_z;

    Vector3(double x, double y, double z)
        : m_x(x), m_y(y), m_z(z) {}
    ~Vector3(){};

    double GetLength()
    {
        return sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
    }

    double GetSqrLength()
    {
        return m_x * m_x + m_y + m_y + m_z + m_z;
    }

    double GetDistance(Vector3& a, Vector3& b)
    {
        return (a - b).GetLength();
    }

    double GetSqrDistance(Vector3& a, Vector3& b)
    {
        return (a - b).GetSqrLength();
    }

    Vector3 operator+(const Vector3& a)
    {
        return Vector3(m_x + a.m_x, m_y + a.m_y, m_z + m_z);
    }

    Vector3 operator-(const Vector3& a)
    {
        return Vector3(m_x - a.m_x, m_y - a.m_y, m_z - a.m_z);
    }

    void operator+=(const Vector3& a)
    {
        *this = *this + a;
    }

    void operator-=(const Vector3& a)
    {
        *this = *this - a;
    }
};

static int Clamp(const int& a, const int& min, const int& max)
{
    if(a > max)
        return max;
    if(a < min)
        return min;
    
    return a;
}

static double Clamp(const double& a, const double& min, const double& max)
{
    if(a > max)
        return max;

    if(a < min)
        return min;
    
    return a;
}