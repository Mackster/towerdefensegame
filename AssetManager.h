#pragma once

#include "Core.h"
#include <string>
#include <sstream>
#include <vector>
#include <map>

class AssetManager
{
private:
    std::map<std::string, TTF_Font *> m_fonts;

public:
    AssetManager(){};
    ~AssetManager(){};

    TTF_Font *GetFont(const std::string &name, const int &size)
    {
        std::string key = name + std::to_string(size);

        std::map<std::string, TTF_Font *>::iterator it = m_fonts.find(key);

        if (it != m_fonts.end())
        {
            return m_fonts[key];
        }

        TTF_Font *temp = TTF_OpenFont(name.c_str(), size);
        m_fonts.emplace(key, temp);

        return temp;
    }
};

static AssetManager FontManager;