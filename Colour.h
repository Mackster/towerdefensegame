#pragma once
#include "Core.h"
#include <sstream>
#include "iostream"

class Colour
{
private:
public:
    ubyte r = 0, g = 0, b = 0, a = 255;

    Colour();
    //Create a new colour with its RGB values
    Colour(ubyte _r, ubyte _g, ubyte _b);
    //Crete a new colour with its RGB values and an alpha value
    Colour(ubyte _r, ubyte _g, ubyte _b, ubyte _a);
    ~Colour();

    SDL_Color ToSDLColor() { return SDL_Color{r, g, b, a}; }

    std::string ToString()
    {
        std::stringstream s;
        s << "Red: " << std::to_string(r) << ", Blue: " << std::to_string(b) << " Green: " << std::to_string(g);

        return s.str();
    }
};

typedef Colour Color;

static Colour black = Colour(0, 0, 0);
static Colour white = Colour(255, 255, 255);
static Colour grey = Colour(127, 127, 127);
static Colour red = Colour(255, 0, 0);
static Colour green = Colour(0, 255, 0);
static Colour blue = Colour(0, 0, 255);