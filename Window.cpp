#include "Window.h"
#include <stdio.h>

Window::Window(uint32 width, uint32 height, const char* title)
    : m_width(width), m_height(height), m_title(title)
{
    m_Screen = SDL_CreateWindow(m_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);

    if (m_Screen == NULL)
    {
        printf("Could not initialize Window: %s\n", SDL_GetError());
    }

    m_MainSurface = SDL_GetWindowSurface(m_Screen);
    m_MainRenderer = SDL_CreateRenderer(m_Screen, -1, SDL_RENDERER_ACCELERATED);

    SDL_SetRenderDrawColor(m_MainRenderer, 127, 127, 127, 255);

    FpsInit();
}

Window::Window(uint32 width, uint32 height)
    : m_width(width), m_height(height), m_title("NONAME")
{
    m_Screen = SDL_CreateWindow(m_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);

    if (m_Screen == NULL)
    {
        printf("Could not initialize Window: %s\n", SDL_GetError());
    }

    m_MainSurface = SDL_GetWindowSurface(m_Screen);
    m_MainRenderer = SDL_CreateRenderer(m_Screen, -1, SDL_RENDERER_ACCELERATED);

    SDL_SetRenderDrawColor(m_MainRenderer, 127, 127, 127, 255);

    FpsInit();
}

Window::Window(uint32 width, uint32 height, Colour background)
    : m_width(width), m_height(height), m_title("NONAME")
{
    m_Screen = SDL_CreateWindow(m_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);

    if (m_Screen == NULL)
    {
        printf("Could not initialize Window: %s\n", SDL_GetError());
    }

    m_MainSurface = SDL_GetWindowSurface(m_Screen);
    m_MainRenderer = SDL_CreateRenderer(m_Screen, -1, SDL_RENDERER_ACCELERATED);

    SDL_SetRenderDrawColor(m_MainRenderer, background.r, background.g, background.b, 255);

    FpsInit();
}

Window::Window(uint32 width, uint32 height, const char* title, Colour background)
    : m_width(width), m_height(height), m_title(title)
{
    m_Screen = SDL_CreateWindow(m_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);

    if (m_Screen == NULL)
    {
        printf("Could not initialize Window: %s\n", SDL_GetError());
    }

    m_MainSurface = SDL_GetWindowSurface(m_Screen);
    m_MainRenderer = SDL_CreateRenderer(m_Screen, -1, SDL_RENDERER_ACCELERATED);

    SDL_SetRenderDrawColor(m_MainRenderer, background.r, background.g, background.b, 255);

    FpsInit();
}

Window::~Window()
{
    SDL_FreeSurface(m_MainSurface);
    SDL_DestroyWindow(m_Screen);
}

void Window::Run()
{
    while (Running)
    {
        LAST = NOW;
        NOW = SDL_GetPerformanceCounter();
        DeltaTime = (double)((NOW - LAST)*1000 / (double)SDL_GetPerformanceFrequency());
        DeltaTime *= 0.001;

        FpsThink();

        SDL_RenderClear(m_MainRenderer);

        if (DrawGFX != 0)
        {
            DrawGFX();
        }

        SDL_RenderPresent(m_MainRenderer);

        lastEvent = currentEvent;
        SDL_PollEvent(&currentEvent);
        
        if (UpdateCallback != 0)
        {
            UpdateCallback();
        }

    }
}

bool Window::GetKey(SDL_Keycode key)
{
    return currentEvent.key.keysym.sym == key;
}

Vector2 Window::GetMouseCoords()
{
    int x, y;

    SDL_GetMouseState(&x, &y);

    return Vector2(x, y);
}