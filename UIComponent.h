#pragma once
#include "Core.h"
#include "Math.h"
#include "Constraints.h"
#include "util.h"
#include "AssetManager.h"

#include <vector>
#include <string>
#include <stdio.h>

class UIComponent
{
public:
    UIComponent(const Vector2& _pos, const Vector2& _size)
        : pos(_pos), size(_size) {}

    ~UIComponent(){};

    Vector2 pos = Vec2One;
    Vector2 size = Vec2One;

    std::vector<UIConstraint> Constraints;

    void AddConstraint(const UIConstraint& cons) { Constraints.push_back(cons); }

    void ConstrainComponent()
    {
        for (auto &cons : Constraints)
        {
            std::string type = cons.GetType();

            if (type == "Size")
            {
                size = cons.Constrain(size);
            }

            if (type == "Pos")
            {
                pos = cons.Constrain(pos);
            }
        }
    }

    bool ContainsMouse(Vector2& mousePos)
    {
        return (mousePos.x >= pos.x && mousePos.x <= pos.x + size.x) && (mousePos.y >= pos.y && mousePos.y <= pos.y + size.y);
    }

    bool ZeroSize()
    {
        return ((int)size.x == 0) || ((int)size.y == 0);
    }

    virtual void Render(SDL_Renderer* rend){};
};

class UILabel : public UIComponent
{

public:
    std::string text;
    std::string fontName = "Assets/Arial.ttf";
    int fontSize = 20;
    Colour fontColour = Colour(255, 255, 255);

    UILabel(const std::string& _text, const std::string& _font, const int& _fontSize,const Vector2& _pos,
            const Colour& _fontColour) : UIComponent(_pos, Vec2One), text(_text), fontName(_font), fontSize(_fontSize),
            fontColour(_fontColour) { }

    UILabel() : UIComponent(Vec2Zero, Vec2Zero) {};

    void Render(SDL_Renderer* rend) override
    {
        TTF_Font* font = FontManager.GetFont(fontName, fontSize);

        if (font == nullptr)
        {
            printf("[UILabel::Render] Failed to load font.\n");
            printf("%s\n", SDL_GetError());
            return;
        }

        SDL_Surface* surface = TTF_RenderText_Solid(font, text.c_str(), fontColour.ToSDLColor());

        SDL_Texture* message = SDL_CreateTextureFromSurface(rend, surface);

        SDL_Rect position;
        position.x = pos.x;
        position.y = pos.y;
        position.w = surface->w;
        position.h = surface->h;

        SDL_RenderCopy(rend, message, nullptr, &position);

        SDL_FreeSurface(surface);
        SDL_DestroyTexture(message);
    }
};

class UIPanel : public UIComponent
{
public:
    Colour panelColour;

    UIPanel(const Colour& col, const Vector2& pos, const Vector2& size) : UIComponent(pos, size), panelColour(col) { }

    UIPanel() : UIComponent(Vec2Zero, Vec2Zero) { }

    ~UIPanel() {}

    void Render(SDL_Renderer* rend) override
    {
        if(ZeroSize())
            return;
        
        SDL_Surface* surf = SDL_CreateRGBSurface(0, size.x, size.y, 32, 0, 0, 0, 0);

        if (surf == 0)
        {
            printf("[UIPanel::Render] Failed to create surface %s\n", SDL_GetError());
        }

        SDL_FillRect(surf, NULL, SDL_MapRGBA(surf->format, panelColour.r, panelColour.g, panelColour.b, panelColour.a));

        SDL_Texture* tex = SDL_CreateTextureFromSurface(rend, surf);

        if (tex == 0)
        {
            printf("[UIPanel::Render] Failed to create texture %s\n", SDL_GetError());
            printf("[UIPanel::Render] Size X:%d, Y:%d\n", (int)size.x, (int)size.y);
        }

        SDL_Rect PanelPos{pos.x, pos.y, size.x, size.y};
        SDL_RenderCopy(rend, tex, nullptr, &PanelPos);

        SDL_DestroyTexture(tex);
        SDL_FreeSurface(surf);
    }
};

class UISlider : public UIComponent
{
    double min = 0, max = 100, value = 0;

    UIPanel dark;
    UIPanel light;

    UILabel Text;

public:
    UISlider(const int& _min, const int& _max, const Vector2& pos, const Vector2& size) : UIComponent(pos, size)
    {
        min = _min;
        max = _max;

        dark = UIPanel(black, pos, size);
        light = UIPanel(white, pos, Vector2(0, size.y));

        Text.fontSize = size.y;
        Text.text = ToString(value, 2);
        Text.pos = Vector2(pos.x + size.x + 5, pos.y);
    }

    void Update(double deltaValue)
    {
        value += deltaValue;
        value = Clamp(value, min, max);

        double percent = value / max;

        light.size.x = dark.size.x * percent;

        Text.text = ToString(value, 2);
    }

    void Update(Vector2 mousePos)
    {
        if(!ContainsMouse(mousePos));
            return;

        SlowLog(500, "[UISlider::Update] update");

        Vector2 offset = mousePos - (light.pos + light.size);
        light.size += offset;
        double percent = light.size.x / dark.size.x;

        value = max * percent;

        Text.text = ToString(value, 2);
    }

    void Render(SDL_Renderer* rend)
    {
        if(ZeroSize()) 
            return; 

        dark.Render(rend);
        if(light.size.x != 0)
            light.Render(rend);

        Text.Render(rend);
    }
};