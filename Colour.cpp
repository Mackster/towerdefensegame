#include "Colour.h"

Colour::Colour() {}

Colour::Colour(ubyte _r, ubyte _g, ubyte _b)
    : r(_r), g(_g), b(_b)
{
}

Colour::Colour(ubyte _r, ubyte _g, ubyte _b, ubyte _a)
    : r(_r), g(_g), b(_b), a(_a)
{
}

Colour::~Colour() {}