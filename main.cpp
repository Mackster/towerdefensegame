#include "Core.h"
#include "util.h"
#include "Window.h"
#include "Texture.h"
#include "UIComponent.h"

#include <stdio.h>
#include <sstream>

Window screen(1280, 720, "Game", grey);
UILabel Text("FPS: 0", "Assets/Arial.ttf", 30, Vector2(0, 10), white);
UILabel temp("", "Assets/Arial.ttf", 35, Vector2(300,300), white);
UISlider s(0, 100, Vector2(500,300), Vector2(200, 50));

bool Initialize()
{
    if (SDL_Init(SDL_INIT_VIDEO != 0))
    {
        printf("[INIT] Could not initialize SDL: %s\n", SDL_GetError());
        return false;
    }

    if (SDL_Init(IMG_INIT_PNG) != 0)
    {
        printf("[INIT] Could not initialize SDL_Image: %s\n", SDL_GetError());
        return false;
    }

    if (TTF_Init() != 0)
    {
        printf("[INIT] Could not initialize SDL_TTF: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

void Quit()
{
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
    SDL_QuitSubSystem(IMG_INIT_PNG);
    SDL_Quit();
}

void Update()
{
    //Quit the window of the escape key is presed or if the x is pressed
    if (screen.GetKey(SDLK_ESCAPE) || screen.currentEvent.type == SDL_QUIT)
    {
        screen.Quit();
    }
}

void DrawFPS(SDL_Renderer* rend)
{
    std::stringstream ss;

    ss << "FPS: " << screen.GetFPS();

    Text.text = ss.str();

    Text.Render(rend);
}

void Draw()
{
    Vector2 mouse = screen.GetMouseCoords();

    SDL_Renderer* rend = screen.GetWindowRenderer();

    s.Update(screen.DeltaTime);

    s.Render(rend);

    DrawFPS(rend);
}

int main()
{
    if (!Initialize())
    {
        return 1;
    }

    screen.SetUpdateCallback(&Update);
    screen.SetGFXCallback(&Draw);
    screen.Run();

    Quit();
}